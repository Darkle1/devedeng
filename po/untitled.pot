# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-13 22:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../src/devedeng/add_files.py:44
msgid "Video files"
msgstr ""

#: ../src/devedeng/add_files.py:49 ../src/devedeng/ask_subtitles.py:92
#: ../src/devedeng/dvd_menu.py:146 ../src/devedeng/dvd_menu.py:158
#: ../src/devedeng/opensave.py:48
msgid "All files"
msgstr ""

#: ../src/devedeng/ask_subtitles.py:81
msgid "Subtitle files"
msgstr ""

#: ../src/devedeng/avconv.py:114 ../src/devedeng/ffmpeg.py:114
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr ""

#: ../src/devedeng/avconv.py:117 ../src/devedeng/ffmpeg.py:117
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr ""

#: ../src/devedeng/avconv.py:127
#, python-format
msgid "Converting %(X)s"
msgstr ""

#: ../src/devedeng/avconv.py:507
#, python-format
msgid "Creating menu %(X)d"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:156
#: ../src/devedeng/choose_disc_type.py:167
#: ../src/devedeng/choose_disc_type.py:183
#: ../src/devedeng/choose_disc_type.py:194
#: ../src/devedeng/choose_disc_type.py:205
#: ../src/devedeng/choose_disc_type.py:215
#: ../src/devedeng/choose_disc_type.py:221
#: ../src/devedeng/choose_disc_type.py:227
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:159
#: ../src/devedeng/choose_disc_type.py:170
#: ../src/devedeng/choose_disc_type.py:186
#: ../src/devedeng/choose_disc_type.py:197
#: ../src/devedeng/choose_disc_type.py:208
#: ../src/devedeng/choose_disc_type.py:218
#: ../src/devedeng/choose_disc_type.py:224
#: ../src/devedeng/choose_disc_type.py:230
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:161
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:172
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:188
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:200
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:210
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:233
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/dvdauthor_converter.py:48
msgid "Creating DVD structure"
msgstr ""

#: ../src/devedeng/dvd_menu.py:51 ../src/devedeng/dvd_menu.py:418
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93
msgid "The selected file is a video, not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93 ../src/devedeng/dvd_menu.py:96
msgid "Error"
msgstr ""

#: ../src/devedeng/dvd_menu.py:96
msgid "The selected file is not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:141
msgid "Picture files"
msgstr ""

#: ../src/devedeng/dvd_menu.py:153
msgid "Sound files"
msgstr ""

#: ../src/devedeng/dvd_menu.py:409
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:416
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:31
#, python-format
msgid "Copying file %(X)s"
msgstr ""

#: ../src/devedeng/genisoimage.py:73 ../src/devedeng/mkisofs.py:73
msgid "Creating ISO image"
msgstr ""

#: ../src/devedeng/help.py:37
msgid "Can't open the help files."
msgstr ""

#: ../src/devedeng/mux_dvd_menu.py:34
#, python-format
msgid "Mixing menu %(X)d"
msgstr ""

#: ../src/devedeng/opensave.py:44
msgid "DevedeNG projects"
msgstr ""

#: ../src/devedeng/project.py:231
msgid "Abort the current DVD and exit?"
msgstr ""

#: ../src/devedeng/project.py:231
msgid "Exit DeVeDe"
msgstr ""

#: ../src/devedeng/project.py:285 ../src/devedeng/project.py:738
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:287
msgid "The following files could not be added:"
msgstr ""

#: ../src/devedeng/project.py:288 ../src/devedeng/project.py:746
msgid "Error while adding files"
msgstr ""

#: ../src/devedeng/project.py:299
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr ""

#: ../src/devedeng/project.py:299
msgid "Delete file"
msgstr ""

#: ../src/devedeng/project.py:499
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""

#: ../src/devedeng/project.py:499
msgid "Too many files in the project"
msgstr ""

#: ../src/devedeng/project.py:508
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""

#: ../src/devedeng/project.py:508
msgid "Delete folder"
msgstr ""

#: ../src/devedeng/project.py:643
msgid "Close current project and start a fresh one?"
msgstr ""

#: ../src/devedeng/project.py:643
msgid "New project"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists. Overwrite it?"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists"
msgstr ""

#: ../src/devedeng/project.py:744 ../src/devedeng/project.py:766
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:746
msgid "The following files in the project could not be added again:"
msgstr ""

#: ../src/devedeng/runner.py:90 ../src/devedeng/runner.py:91
msgid "Cancel the current job?"
msgstr ""

#: ../src/devedeng/settings.py:45 ../src/devedeng/settings.py:63
msgid "Use all cores"
msgstr ""

#: ../src/devedeng/settings.py:50
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:57
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:128
msgid "No discs supported"
msgstr ""

#: ../src/devedeng/subtitles_mux.py:43
#, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr ""

#: ../src/devedeng/title.py:32
#, python-format
msgid "Title %(X)d"
msgstr ""

#: ../src/devedeng/vcdimager_converter.py:48
msgid "Creating CD image"
msgstr ""
