# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-27 10:44+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../src/devedeng/add_files.py:45
msgid "Video files"
msgstr ""

#: ../src/devedeng/add_files.py:50 ../src/devedeng/ask_subtitles.py:93
#: ../src/devedeng/dvd_menu.py:147 ../src/devedeng/dvd_menu.py:159
#: ../src/devedeng/opensave.py:49
msgid "All files"
msgstr ""

#: ../src/devedeng/ask_subtitles.py:82
msgid "Subtitle files"
msgstr ""

#: ../src/devedeng/avconv.py:115 ../src/devedeng/ffmpeg.py:115
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr ""

#: ../src/devedeng/avconv.py:118 ../src/devedeng/ffmpeg.py:118
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr ""

#: ../src/devedeng/avconv.py:128
#, python-format
msgid "Converting %(X)s"
msgstr ""

#: ../src/devedeng/avconv.py:508
#, python-format
msgid "Creating menu %(X)d"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:157
#: ../src/devedeng/choose_disc_type.py:168
#: ../src/devedeng/choose_disc_type.py:184
#: ../src/devedeng/choose_disc_type.py:195
#: ../src/devedeng/choose_disc_type.py:206
#: ../src/devedeng/choose_disc_type.py:216
#: ../src/devedeng/choose_disc_type.py:222
#: ../src/devedeng/choose_disc_type.py:228
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:160
#: ../src/devedeng/choose_disc_type.py:171
#: ../src/devedeng/choose_disc_type.py:187
#: ../src/devedeng/choose_disc_type.py:198
#: ../src/devedeng/choose_disc_type.py:209
#: ../src/devedeng/choose_disc_type.py:219
#: ../src/devedeng/choose_disc_type.py:225
#: ../src/devedeng/choose_disc_type.py:231
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:162
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:173
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:189
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:201
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:211
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:234
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/dvdauthor_converter.py:49
msgid "Creating DVD structure"
msgstr ""

#: ../src/devedeng/dvd_menu.py:52 ../src/devedeng/dvd_menu.py:419
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:94
msgid "The selected file is a video, not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:94 ../src/devedeng/dvd_menu.py:97
msgid "Error"
msgstr ""

#: ../src/devedeng/dvd_menu.py:97
msgid "The selected file is not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:142
msgid "Picture files"
msgstr ""

#: ../src/devedeng/dvd_menu.py:154
msgid "Sound files"
msgstr ""

#: ../src/devedeng/dvd_menu.py:410
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:417
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:32
#, python-format
msgid "Copying file %(X)s"
msgstr ""

#: ../src/devedeng/genisoimage.py:69 ../src/devedeng/mkisofs.py:69
msgid "Creating ISO image"
msgstr ""

#: ../src/devedeng/help.py:20
msgid "Can't open the help files."
msgstr ""

#: ../src/devedeng/mux_dvd_menu.py:35
#, python-format
msgid "Mixing menu %(X)d"
msgstr ""

#: ../src/devedeng/opensave.py:45
msgid "DevedeNG projects"
msgstr ""

#: ../src/devedeng/project.py:232
msgid "Abort the current DVD and exit?"
msgstr ""

#: ../src/devedeng/project.py:232
msgid "Exit DeVeDe"
msgstr ""

#: ../src/devedeng/project.py:286 ../src/devedeng/project.py:736
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:288
msgid "The following files could not be added:"
msgstr ""

#: ../src/devedeng/project.py:289 ../src/devedeng/project.py:744
msgid "Error while adding files"
msgstr ""

#: ../src/devedeng/project.py:300
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr ""

#: ../src/devedeng/project.py:300
msgid "Delete file"
msgstr ""

#: ../src/devedeng/project.py:500
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""

#: ../src/devedeng/project.py:500
msgid "Too many files in the project"
msgstr ""

#: ../src/devedeng/project.py:509
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""

#: ../src/devedeng/project.py:509
msgid "Delete folder"
msgstr ""

#: ../src/devedeng/project.py:641
msgid "Close current project and start a fresh one?"
msgstr ""

#: ../src/devedeng/project.py:641
msgid "New project"
msgstr ""

#: ../src/devedeng/project.py:676
msgid "The file already exists. Overwrite it?"
msgstr ""

#: ../src/devedeng/project.py:676
msgid "The file already exists"
msgstr ""

#: ../src/devedeng/project.py:742 ../src/devedeng/project.py:764
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:744
msgid "The following files in the project could not be added again:"
msgstr ""

#: ../src/devedeng/runner.py:91 ../src/devedeng/runner.py:92
msgid "Cancel the current job?"
msgstr ""

#: ../src/devedeng/settings.py:46 ../src/devedeng/settings.py:64
msgid "Use all cores"
msgstr ""

#: ../src/devedeng/settings.py:51
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:58
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:129
msgid "No discs supported"
msgstr ""

#: ../src/devedeng/subtitles_mux.py:44
#, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr ""

#: ../src/devedeng/title.py:33
#, python-format
msgid "Title %(X)d"
msgstr ""

#: ../src/devedeng/vcdimager_converter.py:49
msgid "Creating CD image"
msgstr ""

#: ../data/interface/wabout.ui:13
msgid "2014 Raster Software Vigo"
msgstr ""

#: ../data/interface/wadd_files.ui:9
msgid "Add new file(s)"
msgstr ""

#: ../data/interface/wask_subtitles.ui:85
msgid "File:"
msgstr ""

#: ../data/interface/wask_subtitles.ui:121
msgid "Encoding:"
msgstr ""

#: ../data/interface/wask_subtitles.ui:166
msgid "Language:"
msgstr ""

#: ../data/interface/wask_subtitles.ui:203
msgid "Put subtitles upper"
msgstr ""

#: ../data/interface/wcreate.ui:9
msgid "devede"
msgstr ""

#: ../data/interface/wcreate.ui:68
msgid ""
"Choose the folder where Devede will create the files and a name for it. Do "
"not use a folder in a VFAT/FAT32 drive. Characters /, | and \\ are not "
"accepted and will be replaced by underscores."
msgstr ""

#: ../data/interface/wcreate.ui:106
msgid "Shutdown computer when disc is done"
msgstr ""

#: ../data/interface/wdone.ui:22
msgid "Burn"
msgstr ""

#: ../data/interface/wdone.ui:64
msgid "Job done!"
msgstr ""

#: ../data/interface/wdone.ui:147 ../data/interface/werror.ui:122
msgid "Debugging data"
msgstr ""

#: ../data/interface/werror.ui:53
msgid "There was an error while creating the disc."
msgstr ""

#: ../data/interface/wfile_properties.ui:116
msgid "Show this title in the menu"
msgstr ""

#: ../data/interface/wfile_properties.ui:139
msgid "<b>Title</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:174
msgid "Path:"
msgstr ""

#: ../data/interface/wfile_properties.ui:214
msgid "Original size (pixels):"
msgstr ""

#: ../data/interface/wfile_properties.ui:237
msgid "Original length (seconds):"
msgstr ""

#: ../data/interface/wfile_properties.ui:261
msgid "Original aspect ratio:"
msgstr ""

#: ../data/interface/wfile_properties.ui:274
msgid "Frames per second:"
msgstr ""

#: ../data/interface/wfile_properties.ui:332
msgid "Original audio rate (Kbits/sec):"
msgstr ""

#: ../data/interface/wfile_properties.ui:345
msgid "Original video rate (Kbits/sec):"
msgstr ""

#: ../data/interface/wfile_properties.ui:368
msgid "<b>File info</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:413 ../data/interface/wmessage.ui:87
msgid "column"
msgstr ""

#: ../data/interface/wfile_properties.ui:439
msgid "Select all"
msgstr ""

#: ../data/interface/wfile_properties.ui:453
msgid "Unselect all"
msgstr ""

#: ../data/interface/wfile_properties.ui:480
msgid "<b>Set properties to these files</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:533
msgid "PAL/SECAM"
msgstr ""

#: ../data/interface/wfile_properties.ui:549 ../data/interface/wmain.ui:802
msgid "NTSC"
msgstr ""

#: ../data/interface/wfile_properties.ui:571
msgid "<b>Video format</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:619
msgid "300"
msgstr ""

#: ../data/interface/wfile_properties.ui:631
msgid "Reset"
msgstr ""

#: ../data/interface/wfile_properties.ui:649
msgid "<b>Volume (%)</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:680
#: ../data/interface/wfile_properties.ui:750
#: ../data/interface/wfile_properties.ui:1194
#: ../data/interface/wfile_properties.ui:1335
#: ../data/interface/wfile_properties.ui:1507
msgid "Automatic"
msgstr ""

#: ../data/interface/wfile_properties.ui:699
msgid "5000"
msgstr ""

#: ../data/interface/wfile_properties.ui:719
msgid "<b>Video rate (KBits/sec)</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:768
msgid "224"
msgstr ""

#: ../data/interface/wfile_properties.ui:788
msgid "<b>Audio rate (KBits/sec)</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:821
msgid "5"
msgstr ""

#: ../data/interface/wfile_properties.ui:836
msgid "Size (in minutes) for each chapter:"
msgstr ""

#: ../data/interface/wfile_properties.ui:846
msgid "Split the file in chapters (for easy seeking)"
msgstr ""

#: ../data/interface/wfile_properties.ui:865
msgid "Manual chapter list (split by comma in mm:ss format):"
msgstr ""

#: ../data/interface/wfile_properties.ui:891
msgid "<b>Division in chapters</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:908
msgid "General"
msgstr ""

#: ../data/interface/wfile_properties.ui:938
msgid "Language"
msgstr ""

#: ../data/interface/wfile_properties.ui:949
msgid "Subtitle"
msgstr ""

#: ../data/interface/wfile_properties.ui:975
msgid "Font size:"
msgstr ""

#: ../data/interface/wfile_properties.ui:997
msgid "Force subtitles"
msgstr ""

#: ../data/interface/wfile_properties.ui:1067
msgid "Fill color:"
msgstr ""

#: ../data/interface/wfile_properties.ui:1092
msgid "Outline color:"
msgstr ""

#: ../data/interface/wfile_properties.ui:1117
msgid "Outline thickness:"
msgstr ""

#: ../data/interface/wfile_properties.ui:1155
msgid "Subtitles"
msgstr ""

#: ../data/interface/wfile_properties.ui:1209
#: ../data/interface/wfile_properties.ui:1350
msgid "1920x1080"
msgstr ""

#: ../data/interface/wfile_properties.ui:1224
#: ../data/interface/wfile_properties.ui:1365
msgid "1280x720"
msgstr ""

#: ../data/interface/wfile_properties.ui:1239
msgid "720x480"
msgstr ""

#: ../data/interface/wfile_properties.ui:1254
msgid "704x480"
msgstr ""

#: ../data/interface/wfile_properties.ui:1269
msgid "480x480"
msgstr ""

#: ../data/interface/wfile_properties.ui:1284
msgid "352x480"
msgstr ""

#: ../data/interface/wfile_properties.ui:1299
msgid "352x240"
msgstr ""

#: ../data/interface/wfile_properties.ui:1380
msgid "720x576"
msgstr ""

#: ../data/interface/wfile_properties.ui:1395
msgid "704x576"
msgstr ""

#: ../data/interface/wfile_properties.ui:1410
msgid "480x576"
msgstr ""

#: ../data/interface/wfile_properties.ui:1425
msgid "352x576"
msgstr ""

#: ../data/interface/wfile_properties.ui:1440
msgid "352x288"
msgstr ""

#: ../data/interface/wfile_properties.ui:1478
msgid "<b>Final size</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1523
msgid "4:3"
msgstr ""

#: ../data/interface/wfile_properties.ui:1540
msgid "16:9"
msgstr ""

#: ../data/interface/wfile_properties.ui:1562
msgid "<b>Aspect ratio</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1591
msgid "Horizontal mirror"
msgstr ""

#: ../data/interface/wfile_properties.ui:1606
msgid "Vertical mirror"
msgstr ""

#: ../data/interface/wfile_properties.ui:1627
msgid "<b>Mirror</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1667
msgid "No rotation"
msgstr ""

#: ../data/interface/wfile_properties.ui:1682
msgid "90 degrees (clockwise)"
msgstr ""

#: ../data/interface/wfile_properties.ui:1697
msgid "180 degrees"
msgstr ""

#: ../data/interface/wfile_properties.ui:1712
msgid "90 degrees (counter-clockwise)"
msgstr ""

#: ../data/interface/wfile_properties.ui:1733
msgid "<b>Rotation</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1761
msgid "Add black bars"
msgstr ""

#: ../data/interface/wfile_properties.ui:1776
msgid "Scale picture"
msgstr ""

#: ../data/interface/wfile_properties.ui:1791
msgid "Cut picture"
msgstr ""

#: ../data/interface/wfile_properties.ui:1845
msgid "<b>Scaling mode</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1872
msgid "Video options"
msgstr ""

#: ../data/interface/wfile_properties.ui:1897
msgid "Use dual pass encoding (better quality, much slower conversion)"
msgstr ""

#: ../data/interface/wfile_properties.ui:1911
msgid "<b>Two pass encoding</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:1940
msgid "Don't deinterlace"
msgstr ""

#: ../data/interface/wfile_properties.ui:1956
msgid "FFMpeg deinterlacing filter"
msgstr ""

#: ../data/interface/wfile_properties.ui:1973
msgid "YADIF deinterlacing filter"
msgstr ""

#: ../data/interface/wfile_properties.ui:1996
msgid "<b>Deinterlacing</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:2016
msgid "Quality"
msgstr ""

#: ../data/interface/wfile_properties.ui:2047
msgid "Audio delay (in seconds):"
msgstr ""

#: ../data/interface/wfile_properties.ui:2077
msgid "Create DVD with 5.1 channel sound"
msgstr ""

#: ../data/interface/wfile_properties.ui:2092
msgid ""
"This file already has AC3 sound (copy audio data instead of recompress it)"
msgstr ""

#: ../data/interface/wfile_properties.ui:2113
msgid "<b>Audio</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:2126
msgid "Audio"
msgstr ""

#: ../data/interface/wfile_properties.ui:2151
#: ../data/interface/wtitle_properties.ui:119
msgid "Stop reproduction/show disc menu"
msgstr ""

#: ../data/interface/wfile_properties.ui:2167
#: ../data/interface/wtitle_properties.ui:135
msgid "Play the first title"
msgstr ""

#: ../data/interface/wfile_properties.ui:2183
#: ../data/interface/wtitle_properties.ui:152
msgid "Play the previous title"
msgstr ""

#: ../data/interface/wfile_properties.ui:2199
#: ../data/interface/wtitle_properties.ui:169
msgid "Play this title again (loop)"
msgstr ""

#: ../data/interface/wfile_properties.ui:2215
#: ../data/interface/wtitle_properties.ui:186
msgid "Play the next title"
msgstr ""

#: ../data/interface/wfile_properties.ui:2231
#: ../data/interface/wtitle_properties.ui:203
msgid "Play the last title"
msgstr ""

#: ../data/interface/wfile_properties.ui:2253
#: ../data/interface/wtitle_properties.ui:226
msgid "<b>Action to perform when this title ends</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:2266
msgid "Actions"
msgstr ""

#: ../data/interface/wfile_properties.ui:2291
msgid "This file is already a DVD/xCD-suitable MPEG-PS file"
msgstr ""

#: ../data/interface/wfile_properties.ui:2306
msgid "Repack audio and video without reencoding (useful for VOB files)"
msgstr ""

#: ../data/interface/wfile_properties.ui:2321
msgid "Use a GOP of 12 frames (improves compatibility)"
msgstr ""

#: ../data/interface/wfile_properties.ui:2342
msgid "<b>Special</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:2355
msgid "Misc"
msgstr ""

#: ../data/interface/wfile_properties.ui:2370
msgid "<b>Options</b>"
msgstr ""

#: ../data/interface/wfile_properties.ui:2383 ../data/interface/wmain.ui:567
msgid "Preview"
msgstr ""

#: ../data/interface/wmain.ui:73
msgid "_File"
msgstr ""

#: ../data/interface/wmain.ui:143
msgid "_Edit"
msgstr ""

#: ../data/interface/wmain.ui:153
msgid "Set multiproperties"
msgstr ""

#: ../data/interface/wmain.ui:176
msgid "_Help"
msgstr ""

#: ../data/interface/wmain.ui:186
msgid "Show documentation"
msgstr ""

#: ../data/interface/wmain.ui:248
msgid "Title"
msgstr ""

#: ../data/interface/wmain.ui:263
msgid "Duration"
msgstr ""

#: ../data/interface/wmain.ui:338
msgid "Add file"
msgstr ""

#: ../data/interface/wmain.ui:384
msgid "Remove"
msgstr ""

#: ../data/interface/wmain.ui:430
msgid "Move down"
msgstr ""

#: ../data/interface/wmain.ui:476
msgid "Move up"
msgstr ""

#: ../data/interface/wmain.ui:522
msgid "Properties"
msgstr ""

#: ../data/interface/wmain.ui:612
msgid "Add separator"
msgstr ""

#: ../data/interface/wmain.ui:644
msgid "<b>Files</b>"
msgstr ""

#: ../data/interface/wmain.ui:675
msgid "Media size:"
msgstr ""

#: ../data/interface/wmain.ui:723
msgid "Adjust disc usage"
msgstr ""

#: ../data/interface/wmain.ui:750
msgid "<b>Disc usage</b>"
msgstr ""

#: ../data/interface/wmain.ui:783
msgid "PAL"
msgstr ""

#: ../data/interface/wmain.ui:825
msgid "<b>Default format</b>"
msgstr ""

#: ../data/interface/wmain.ui:853
msgid "Create a menu with the titles"
msgstr ""

#: ../data/interface/wmain.ui:868
msgid "Menu options"
msgstr ""

#: ../data/interface/wmain.ui:887
msgid "<b>Menus</b>"
msgstr ""

#: ../data/interface/wmenu.ui:95
msgid "Text:"
msgstr ""

#: ../data/interface/wmenu.ui:107 ../data/interface/wmenu.ui:693
msgid "Font:"
msgstr ""

#: ../data/interface/wmenu.ui:119
msgid "Text color:"
msgstr ""

#: ../data/interface/wmenu.ui:131
msgid "Shadow color:"
msgstr ""

#: ../data/interface/wmenu.ui:190
msgid "Vertical position (percent):"
msgstr ""

#: ../data/interface/wmenu.ui:203 ../data/interface/wmenu.ui:230
#: ../data/interface/wmenu.ui:596 ../data/interface/wmenu.ui:611
#: ../data/interface/wmenu.ui:625 ../data/interface/wmenu.ui:639
msgid "0,00"
msgstr ""

#: ../data/interface/wmenu.ui:217
msgid "Horizontal position (percent):"
msgstr ""

#: ../data/interface/wmenu.ui:248
msgid "<b>Menu title</b>"
msgstr ""

#: ../data/interface/wmenu.ui:287
msgid "Default background"
msgstr ""

#: ../data/interface/wmenu.ui:307
msgid "<b>Menu background</b>"
msgstr ""

#: ../data/interface/wmenu.ui:351
msgid "Menu without sound"
msgstr ""

#: ../data/interface/wmenu.ui:379
msgid "Audio format:"
msgstr ""

#: ../data/interface/wmenu.ui:389
msgid "MP2"
msgstr ""

#: ../data/interface/wmenu.ui:405
msgid "AC3"
msgstr ""

#: ../data/interface/wmenu.ui:434
msgid "<b>Menu music</b>"
msgstr ""

#: ../data/interface/wmenu.ui:462
msgid "4:3 (classic)"
msgstr ""

#: ../data/interface/wmenu.ui:477
msgid "16:9 (widescreen)"
msgstr ""

#: ../data/interface/wmenu.ui:498
msgid "<b>Menu aspect</b>"
msgstr ""

#: ../data/interface/wmenu.ui:529
msgid "<b>Horizontal</b>"
msgstr ""

#: ../data/interface/wmenu.ui:541
msgid "<b>Margins (percent)</b>"
msgstr ""

#: ../data/interface/wmenu.ui:552
msgid "Left"
msgstr ""

#: ../data/interface/wmenu.ui:566
msgid "Center"
msgstr ""

#: ../data/interface/wmenu.ui:580
msgid "Right"
msgstr ""

#: ../data/interface/wmenu.ui:658
msgid "<b>Menu position</b>"
msgstr ""

#: ../data/interface/wmenu.ui:742
msgid "Unselected:"
msgstr ""

#: ../data/interface/wmenu.ui:754 ../data/interface/wmenu.ui:882
msgid "Shadows:"
msgstr ""

#: ../data/interface/wmenu.ui:766
msgid "Selected:"
msgstr ""

#: ../data/interface/wmenu.ui:778 ../data/interface/wmenu.ui:929
msgid "Background:"
msgstr ""

#: ../data/interface/wmenu.ui:839
msgid "<b>Titles</b>"
msgstr ""

#: ../data/interface/wmenu.ui:870
msgid "Color:"
msgstr ""

#: ../data/interface/wmenu.ui:964
msgid "<b>Separators</b>"
msgstr ""

#: ../data/interface/wmenu.ui:983
msgid "<b>Menu font and colors</b>"
msgstr ""

#: ../data/interface/wmenu.ui:1012
msgid "Show menu at disk startup"
msgstr ""

#: ../data/interface/wmenu.ui:1028
msgid "Jump to the first title at startup"
msgstr ""

#: ../data/interface/wmenu.ui:1048
msgid "Provide \"Play All\" option"
msgstr ""

#: ../data/interface/wmenu.ui:1087
msgid "<b>Disc startup options</b>"
msgstr ""

#: ../data/interface/wmenu.ui:1116
msgid "Show titles as selected"
msgstr ""

#: ../data/interface/wmenu.ui:1190
msgid "<b>Preview</b>"
msgstr ""

#: ../data/interface/wmenu.ui:1255
msgid "<b>Menu preview</b>"
msgstr ""

#: ../data/interface/wneeded.ui:53
msgid "Programs needed by Devede NG"
msgstr ""

#: ../data/interface/wpreview.ui:69
msgid ""
"<b>Preview video</b>\n"
"\n"
"Devede will create a preview with the selected parameters, so you will be "
"able to check the video quality, audio sync and so on."
msgstr ""

#: ../data/interface/wpreview.ui:90
msgid "Preview length:"
msgstr ""

#: ../data/interface/wprogress.ui:8
msgid "Creating..."
msgstr ""

#: ../data/interface/wprogress.ui:24
msgid "Creating disc"
msgstr ""

#: ../data/interface/wprogress.ui:70
msgid "Project progress"
msgstr ""

#: ../data/interface/wselect_disk.ui:7
msgid "Devede NG"
msgstr ""

#: ../data/interface/wselect_disk.ui:22
msgid "<b>Choose the disc type you want to create with Devede</b>"
msgstr ""

#: ../data/interface/wselect_disk.ui:58
msgid ""
"<b>Video DVD</b>\n"
"Creates a video DVD suitable for all DVD home players"
msgstr ""

#: ../data/interface/wselect_disk.ui:105
msgid ""
"<b>VideoCD</b>\n"
"Creates a VideoCD, with a picture quality equivalent to VHS"
msgstr ""

#: ../data/interface/wselect_disk.ui:152
msgid ""
"<b>Super VideoCD</b>\n"
"Creates a VideoCD with better picture quality"
msgstr ""

#: ../data/interface/wselect_disk.ui:199
msgid ""
"<b>China VideoDisc</b>\n"
"Another kind of Super VideoCD"
msgstr ""

#: ../data/interface/wselect_disk.ui:246
msgid ""
"<b>DivX / MPEG-4</b>\n"
"Creates files compliant with DivX home players"
msgstr ""

#: ../data/interface/wselect_disk.ui:293
msgid ""
"<b>Matroska / H.264</b>\n"
"Creates H.264 files in a MKV container"
msgstr ""

#: ../data/interface/wselect_disk.ui:321
msgid "Programs needed by Devede"
msgstr ""

#: ../data/interface/wsettings.ui:134
msgid "<b>Multicore CPUs</b>"
msgstr ""

#: ../data/interface/wsettings.ui:164
msgid "Temporary files folder:"
msgstr ""

#: ../data/interface/wsettings.ui:193
msgid "<b>Folders</b>"
msgstr ""

#: ../data/interface/wsettings.ui:225
msgid "Play preview:"
msgstr ""

#: ../data/interface/wsettings.ui:240
msgid "Get video info:"
msgstr ""

#: ../data/interface/wsettings.ui:255
msgid "Convert menus:"
msgstr ""

#: ../data/interface/wsettings.ui:270
msgid "Convert videos:"
msgstr ""

#: ../data/interface/wsettings.ui:364
msgid "Burn ISOs:"
msgstr ""

#: ../data/interface/wsettings.ui:397
msgid "Disc types supported:"
msgstr ""

#: ../data/interface/wsettings.ui:424
msgid "Create ISOs:"
msgstr ""

#: ../data/interface/wsettings.ui:460
msgid "<b>Backends</b>"
msgstr ""

#: ../data/interface/wtitle_properties.ui:8
msgid "Title properties"
msgstr ""

#: ../data/interface/wtitle_properties.ui:90
msgid "<b>Title's name</b>"
msgstr ""
