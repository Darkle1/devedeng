# translation of pl.po to
# qla <qla0@vp.pl>, 2008.
# Copyright (C) 2006 THE DeVeDe'S COPYRIGHT HOLDER
# This file is distributed under the same license as the DeVeDe package.
msgid ""
msgstr ""
"Project-Id-Version: pl\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-13 22:07+0200\n"
"PO-Revision-Date: 2011-12-10 21:32+0100\n"
"Last-Translator: sunmake <sunmake@wp.pl>\n"
"Language-Team:  <pl@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: ../src/devedeng/add_files.py:44
msgid "Video files"
msgstr "Pliki wideo"

#: ../src/devedeng/add_files.py:49 ../src/devedeng/ask_subtitles.py:92
#: ../src/devedeng/dvd_menu.py:146 ../src/devedeng/dvd_menu.py:158
#: ../src/devedeng/opensave.py:48
msgid "All files"
msgstr "Wszystkie pliki"

#: ../src/devedeng/ask_subtitles.py:81
#, fuzzy
msgid "Subtitle files"
msgstr "Napisy"

#: ../src/devedeng/avconv.py:114 ../src/devedeng/ffmpeg.py:114
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr ""

#: ../src/devedeng/avconv.py:117 ../src/devedeng/ffmpeg.py:117
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr ""

#: ../src/devedeng/avconv.py:127
#, python-format
msgid "Converting %(X)s"
msgstr ""

#: ../src/devedeng/avconv.py:507
#, fuzzy, python-format
msgid "Creating menu %(X)d"
msgstr "Tworzenie menu %(menu_number)d"

#: ../src/devedeng/choose_disc_type.py:156
#: ../src/devedeng/choose_disc_type.py:167
#: ../src/devedeng/choose_disc_type.py:183
#: ../src/devedeng/choose_disc_type.py:194
#: ../src/devedeng/choose_disc_type.py:205
#: ../src/devedeng/choose_disc_type.py:215
#: ../src/devedeng/choose_disc_type.py:221
#: ../src/devedeng/choose_disc_type.py:227
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:159
#: ../src/devedeng/choose_disc_type.py:170
#: ../src/devedeng/choose_disc_type.py:186
#: ../src/devedeng/choose_disc_type.py:197
#: ../src/devedeng/choose_disc_type.py:208
#: ../src/devedeng/choose_disc_type.py:218
#: ../src/devedeng/choose_disc_type.py:224
#: ../src/devedeng/choose_disc_type.py:230
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:161
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:172
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:188
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:200
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:210
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:233
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/dvdauthor_converter.py:48
#, fuzzy
msgid "Creating DVD structure"
msgstr "Tworzenie struktury DVD"

#: ../src/devedeng/dvd_menu.py:51 ../src/devedeng/dvd_menu.py:418
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93
msgid "The selected file is a video, not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93 ../src/devedeng/dvd_menu.py:96
msgid "Error"
msgstr "Błąd"

#: ../src/devedeng/dvd_menu.py:96
#, fuzzy
msgid "The selected file is not an audio file"
msgstr "Plik wydaje się być plikiem dźwiękowym."

#: ../src/devedeng/dvd_menu.py:141
#, fuzzy
msgid "Picture files"
msgstr "Pliki wideo"

#: ../src/devedeng/dvd_menu.py:153
#, fuzzy
msgid "Sound files"
msgstr "Pliki wideo"

#: ../src/devedeng/dvd_menu.py:409
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:416
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:31
#, fuzzy, python-format
msgid "Copying file %(X)s"
msgstr "Kopiowanie pliku"

#: ../src/devedeng/genisoimage.py:73 ../src/devedeng/mkisofs.py:73
#, fuzzy
msgid "Creating ISO image"
msgstr "Tworzenie pliku ISO"

#: ../src/devedeng/help.py:37
#, fuzzy
msgid "Can't open the help files."
msgstr "Nie można otworzyć pliku."

#: ../src/devedeng/mux_dvd_menu.py:34
#, python-format
msgid "Mixing menu %(X)d"
msgstr ""

#: ../src/devedeng/opensave.py:44
msgid "DevedeNG projects"
msgstr ""

#: ../src/devedeng/project.py:231
msgid "Abort the current DVD and exit?"
msgstr "Przerwać tworzenie dysku i wyjść?"

#: ../src/devedeng/project.py:231
msgid "Exit DeVeDe"
msgstr "Zakończ DeVeDe"

#: ../src/devedeng/project.py:285 ../src/devedeng/project.py:738
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:287
msgid "The following files could not be added:"
msgstr ""

#: ../src/devedeng/project.py:288 ../src/devedeng/project.py:746
msgid "Error while adding files"
msgstr ""

#: ../src/devedeng/project.py:299
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr ""

#: ../src/devedeng/project.py:299
#, fuzzy
msgid "Delete file"
msgstr "Usuń tytuł"

#: ../src/devedeng/project.py:499
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""

#: ../src/devedeng/project.py:499
msgid "Too many files in the project"
msgstr ""

#: ../src/devedeng/project.py:508
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""

#: ../src/devedeng/project.py:508
#, fuzzy
msgid "Delete folder"
msgstr "Usuń tytuł"

#: ../src/devedeng/project.py:643
msgid "Close current project and start a fresh one?"
msgstr ""

#: ../src/devedeng/project.py:643
msgid "New project"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists. Overwrite it?"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists"
msgstr ""

#: ../src/devedeng/project.py:744 ../src/devedeng/project.py:766
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:746
msgid "The following files in the project could not be added again:"
msgstr ""

#: ../src/devedeng/runner.py:90 ../src/devedeng/runner.py:91
#, fuzzy
msgid "Cancel the current job?"
msgstr "Anulować zadanie?"

#: ../src/devedeng/settings.py:45 ../src/devedeng/settings.py:63
msgid "Use all cores"
msgstr ""

#: ../src/devedeng/settings.py:50
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: ../src/devedeng/settings.py:57
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: ../src/devedeng/settings.py:128
msgid "No discs supported"
msgstr ""

#: ../src/devedeng/subtitles_mux.py:43
#, fuzzy, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr "Dodawanie napisów do"

#: ../src/devedeng/title.py:32
#, python-format
msgid "Title %(X)d"
msgstr "Tytuł %(X)d"

#: ../src/devedeng/vcdimager_converter.py:48
#, fuzzy
msgid "Creating CD image"
msgstr "Tworzenie pliku ISO"

#~ msgid "Creating BIN/CUE files"
#~ msgstr "Tworzenie pliku BIN/CUE"

#~ msgid ""
#~ "Failed to create the BIN/CUE files\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Nieudane tworzenie pliku BIN/CUE\n"
#~ "Może miejsce na dysku wyczerpało się"

#~ msgid ""
#~ "Failed to create the ISO image\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Nieudane tworzenie obrazu ISO\n"
#~ "Może miejsce na dysku wyczerpało się"

#~ msgid ""
#~ "Insuficient free space. To create this disc\n"
#~ "%(total)d MBytes are needed, but only %(free)d MBytes are available."
#~ msgstr ""
#~ "Niedostateczna ilość wolnego miejsca .Do utworzenia\n"
#~ "potrzeba %(total)d MB, dostępne tylko %(free)d MB."

#~ msgid ""
#~ "Failed to write to the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Błąd zapisu do katalogu docelowego.\n"
#~ "Sprawdź czy jest wolne miejsce i masz uprawnienia do zapisu."

#~ msgid ""
#~ "The file or folder\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "already exists. If you continue, it will be deleted."
#~ msgstr ""
#~ "To jest istniejący plik lub folder\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "Jeżeli będziesz kontynuować,zostanie skasowany."

#~ msgid "Unknown error"
#~ msgstr "Nieznany błąd"

#~ msgid ""
#~ "Failed to create the DVD tree\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Nieudane tworzenie struktury plików DVD\n"
#~ "Może miejsce na dysku wyczerpało się"

#~ msgid ""
#~ "The menu soundtrack seems damaged. Using the default silent soundtrack."
#~ msgstr ""
#~ "Dźwięk menu wygląda na uszkodzony. Użyte zostaną domyślne ustawienia (bez "
#~ "dźwięku)."

#~ msgid ""
#~ "Can't find the menu background.\n"
#~ "Check the menu options."
#~ msgstr ""
#~ "Nie można znaleźć tła menu.\n"
#~ "Sprawdź opcje menu."

#~ msgid "That file doesn't contain a disc structure."
#~ msgstr "Ten plik nie zawiera struktury dysku."

#~ msgid "That file doesn't contain a DeVeDe structure."
#~ msgstr "Ten plik nie zawiera struktury DeVeDe."

#~ msgid ""
#~ "Can't find the following movie files. Please, add them and try to load "
#~ "the disc structure again.\n"
#~ msgstr ""
#~ "Nie można znaleźć następujących plików z filmami. Proszę je dodać i "
#~ "spróbować załadować strukturę dysku jeszcze raz.\n"

#~ msgid ""
#~ "Can't find the menu background. I'll open the disc structure anyway with "
#~ "the default menu background, so don't forget to fix it before creating "
#~ "the disc."
#~ msgstr ""
#~ "Nie można znaleźć tła menu. Utworzę strukturę dysku z domyślnym tłem "
#~ "menu, więc nie zapomnij to naprawić zanim utworzysz dysk."

#~ msgid ""
#~ "Can't find the menu soundtrack file. I'll open the disc structure anyway "
#~ "with a silent soundtrack, so don't forget to fix it before creating the "
#~ "disc."
#~ msgstr ""
#~ "Nie można odnaleźć pliku dźwiękowego menu. Utworzę więc strukturę dysku "
#~ "bez dźwięku Nie zapomnij to naprawić zanim utworzysz dysk."

#~ msgid "No filename"
#~ msgstr "Brak nazwy pliku"

#~ msgid "Can't save the file."
#~ msgstr "Nie można zapisać pliku."

#~ msgid ""
#~ "Too many videos for this disc size.\n"
#~ "Please, select a bigger disc type or remove some videos."
#~ msgstr ""
#~ "Za dużo wideo dla tej wielkości dysku.\n"
#~ "Proszę, wybierać większy dysk lub usunąć jakieś pliki wideo."

#~ msgid ""
#~ "Some files weren't video files.\n"
#~ "None added."
#~ msgstr ""
#~ "Niektóre pliki nie były plikami wideo\n"
#~ "Nic nie dodano."

#~ msgid ""
#~ "Your project contains %(X)d movie files, but the maximum is %(MAX)d. "
#~ "Please, remove some files and try again."
#~ msgstr ""
#~ "Twój projekt zawiera %(X)d plików z filmami, ale maksimum to %(MAX)d. "
#~ "Proszę, usunąć jakieś pliki i spróbować jeszcze raz."

#~ msgid "no chapters"
#~ msgstr "brak rozdziałów"

#~ msgid "Unsaved disc structure"
#~ msgstr "Niezapisana struktura dysku"

#~ msgid "Codepage"
#~ msgstr "Kodowanie"

#~ msgid "Language"
#~ msgstr "Język"

#~ msgid "File doesn't seem to be a video file."
#~ msgstr "Plik nie wydaje się być plikiem wideo."

#~ msgid "Please, add only one file each time."
#~ msgstr "Proszę dodawać jeden plik za każdym razem."

#~ msgid "Please, add a movie file before adding subtitles."
#~ msgstr "Proszę dodać plik z filmem przed dodaniem napisów."

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "Konwersja nieudana\n"
#~ "Wydaje się, że jest to błąd SPUMUX."

#~ msgid ""
#~ "File copy failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Kopiowanie pliku nieudane\n"
#~ "Może miejsce na dysku wyczerpało się?"

#~ msgid "Creating preview"
#~ msgstr "Tworzenie podglądu"

#~ msgid ""
#~ "Converting files from title %(title_number)s (pass %(pass_number)s)\n"
#~ "\n"
#~ "%(file_name)s"
#~ msgstr ""
#~ "Konwertowanie plików tytułu %(title_number)s (zakończono "
#~ "%(pass_number)s)\n"
#~ "\n"
#~ "%(file_name)s"

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of Mencoder."
#~ msgstr ""
#~ "Konwersja nieudana\n"
#~ "Wydaje się, że jest to błąd programu Mencoder."

#~ msgid "Also check the extra params passed to Mencoder for syntax errors."
#~ msgstr ""
#~ "Sprawdź dodatkowe parametry programu Mencoder pod kątem błędów "
#~ "składniowych."

#~ msgid ""
#~ "Conversion failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Konwersja nieudana\n"
#~ "Może miejsce na dysku wyczerpało się?"

#~ msgid "Failed to create the menues."
#~ msgstr "Błąd tworzenia menu."

#~ msgid "Menu generation failed."
#~ msgstr "Generowanie menu nieudane."

#~ msgid ""
#~ "Can't add the buttons to the menus.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "Nie można dodać przycisków do menu.\n"
#~ "Wygląda to na błąd SPUMUX."

#~ msgid "Erase temporary files"
#~ msgstr "Kasuj pliki tymczasowe"

#~ msgid "Use optimizations for multicore CPUs"
#~ msgstr "Użyj optymalizacji dla procesorów wielordzeniowych"

#~ msgid "<b>Options</b>"
#~ msgstr "<b>Opcje</b>"

#~ msgid "Temporary files folder:"
#~ msgstr "Folder plików tymczasowych:"

#~ msgid "Folder for temporary file"
#~ msgstr "Katalog dla pliku tymczasowego"

#~ msgid "<b>Folders</b>"
#~ msgstr "<b>Foldery</b>"

#~ msgid ""
#~ "AC3_FIXED workaround to fix discs without sound when using Mencoder beta"
#~ msgstr ""
#~ "Obejście używając AC3_FIX by naprawić dysk bez dźwięku kiedy użyty jest "
#~ "Mencoder beta"

#~ msgid "<b>Workarounds</b>"
#~ msgstr "<b>Rozwiązania</b>"

#~ msgid "File:"
#~ msgstr "Plik:"

#~ msgid "Clear"
#~ msgstr "Wyczyść"

#~ msgid "Encoding:"
#~ msgstr "Kodowanie:"

#~ msgid "Language:"
#~ msgstr "Język:"

#~ msgid "Put subtitles upper"
#~ msgstr "Umieść napisy wyżej"

#~ msgid "Add subtitles"
#~ msgstr "Dodaj napisy"

#~ msgid "Choose a file"
#~ msgstr "Wybierz plik"

#~ msgid "Aborted. There can be temporary files at the destination directory."
#~ msgstr "Przerwane.W katalogu docelowym mogły pozostać pliki tymczasowe."

#~ msgid "Delete chapter"
#~ msgstr "Usuń rozdział"

#~ msgid ""
#~ "You chose to delete this chapter and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Wybrano usunięcie tego rozdziału i \n"
#~ "zmianę kolejności pozostałych:"

#~ msgid "Proceed?"
#~ msgstr "Kontynuować?"

#~ msgid "Delete subtitle"
#~ msgstr "Usuń napisy"

#~ msgid "Remove the selected subtitle file?"
#~ msgstr "Usunąć zaznaczony plik z napisami?"

#~ msgid ""
#~ "You chose to delete this title and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Wybrano usunięcie tego tytułu i\n"
#~ "zmianę kolejności pozostałych:"

#~ msgid "Disc type selection"
#~ msgstr "Wybór typu dysku"

#~ msgid "<b>Choose the disc type you want to create with DeVeDe</b>"
#~ msgstr "<b>Wybierz rodzaj dysku który chcesz utworzyć za pomocą DeVeDe</b>"

#~ msgid ""
#~ "<b>Video DVD</b>\n"
#~ "Creates a video DVD suitable for all DVD home players"
#~ msgstr ""
#~ "<b>Video DVD</b>\n"
#~ "Tworzy video DVD odpowiednie dla domowych odtwarzaczy DVD"

#~ msgid ""
#~ "<b>VideoCD</b>\n"
#~ "Creates a VideoCD, with a picture quality equivalent to VHS"
#~ msgstr ""
#~ "<b>VideoCD</b>\n"
#~ "Tworzy VideoCD, z jakością obrazu równoważną VHS"

#~ msgid ""
#~ "<b>Super VideoCD</b>\n"
#~ "Creates a VideoCD with better picture quality"
#~ msgstr ""
#~ "<b>Super VideoCD</b>\n"
#~ "Tworzy VideoCD z lepszą jakością obrazu"

#~ msgid ""
#~ "<b>China VideoDisc</b>\n"
#~ "Another kind of Super VideoCD"
#~ msgstr ""
#~ "<b>China VideoDisc</b>\n"
#~ "Odmiana Super VideoCD"

#~ msgid ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Creates files compliant with DivX home players"
#~ msgstr ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Tworzy pliki zgodne z DivX odtwarzaczy domowych"

#~ msgid ""
#~ "Your DVD structure contains empty titles.\n"
#~ "Continue anyway?"
#~ msgstr ""
#~ "Ta struktura plików DVD zawiera puste tytuły.\n"
#~ "Kontynuować pomimo to?"

#~ msgid "Job done!"
#~ msgstr "Zadanie wykonane!"

#~ msgid "Delete current disc structure?"
#~ msgstr "Usunąć aktualną strukturę dysku?"

#~ msgid "Delete the current disc structure and start again?"
#~ msgstr "Usunąć aktualną strukturę dysku i rozpocząć ponownie?"

#~ msgid "File properties"
#~ msgstr "Właściwości pliku"

#~ msgid "<b>File</b>"
#~ msgstr "<b>Plik</b>"

#~ msgid "Frames per second:"
#~ msgstr "Klatek na sekundę:"

#~ msgid "Original length (seconds):"
#~ msgstr "Oryginalna długość (sekundy):"

#~ msgid "Original audio rate (Kbits/sec):"
#~ msgstr "Oryginalna szybkość transmisji audio (Kbits/sec):"

#~ msgid "Original video rate (Kbits/sec):"
#~ msgstr "Oryginalna szybkość transmisji wideo (Kbits/sec):"

#~ msgid "Original size (pixels):"
#~ msgstr "Oryginalny rozmiar (piksele):"

#~ msgid "Final size (pixels):"
#~ msgstr "Końcowy rozmiar (piksele):"

#~ msgid "Estimated final length (MBytes):"
#~ msgstr "Przybliżona końcowa wielkość (Megabajty):"

#~ msgid "<b>File info</b>"
#~ msgstr "<b>Informacja o pliku</b>"

#~ msgid "PAL/SECAM"
#~ msgstr "PAL/SECAM"

#~ msgid "NTSC"
#~ msgstr "NTSC"

#~ msgid "<b>Video format</b>"
#~ msgstr "<b>Format wideo</b>"

#~ msgid "Audio track:"
#~ msgstr "Ścieżka dżwiękowa:"

#~ msgid "This video file has no audio tracks."
#~ msgstr "Ten plik wideo nie posiada ścieżek z dźwiękiem"

#~ msgid "<b>Audio tracks</b>"
#~ msgstr "<b>Ścieżki audio</b>"

#~ msgid "Reset"
#~ msgstr "Zresetuj"

#~ msgid "Resets the volume to 100%"
#~ msgstr "Zmień rozmiar do 100%"

#~ msgid "<b>Volume (%)</b>"
#~ msgstr "<b>Rozmiar (%)</b>"

#~ msgid "Store full length"
#~ msgstr "Zachowaj pełną długość"

#~ msgid "Store the first half"
#~ msgstr "Zachowaj pierwszą połowę"

#~ msgid "Store the second half"
#~ msgstr "Zachowaj drugą połowę"

#~ msgid "<b>File split</b>"
#~ msgstr "<b>Dzielenie pliku</b>"

#~ msgid "Font size:"
#~ msgstr "Wielkość czcionki:"

#~ msgid "Force subtitles"
#~ msgstr "Wymuś napisy"

#~ msgid "DivX format doesn't support subtitles. Please, read the FAQ."
#~ msgstr "Format DivX nie wspiera napisów. Proszę przeczytać FAQ."

#~ msgid "<b>Subtitles</b>"
#~ msgstr "<b>Napisy</b>"

#~ msgid "<b>Video rate (Kbits/sec)</b>"
#~ msgstr "<b>Szybkość transmisji Wideo (Kbits/sec)</b>"

#~ msgid "<b>Audio rate (Kbits/sec)</b>"
#~ msgstr "<b>Szybkość transmisji Audio (Kbits/sec)</b>"

#~ msgid "Split the file in chapters (for easy seeking)"
#~ msgstr "Podziel pliki w rozdziałach (dla łatwego wyszukiwania)"

#~ msgid "Size (in minutes) for each chapter:"
#~ msgstr "Wielkość (w minutach) dla każdego rozdziału:"

#~ msgid "<b>Division in chapters</b>"
#~ msgstr "<b>Podział w rozdziałach</b>"

#~ msgid "General"
#~ msgstr "Ogólne"

#~ msgid "352x240"
#~ msgstr "352x240"

#~ msgid "Default"
#~ msgstr "Domyślny"

#~ msgid "352x480"
#~ msgstr "352x480"

#~ msgid "480x480"
#~ msgstr "480x480"

#~ msgid "720x480"
#~ msgstr "720x480"

#~ msgid "704x480"
#~ msgstr "704x480"

#~ msgid "1920x1080"
#~ msgstr "1920x1080"

#~ msgid "1280x720"
#~ msgstr "1280x720"

#~ msgid "160x128"
#~ msgstr "160x128"

#~ msgid "<b>Final size</b>"
#~ msgstr "<b>Końcowy rozmiar</b>"

#~ msgid "4:3"
#~ msgstr "4:3"

#~ msgid "16:9"
#~ msgstr "16:9"

#~ msgid "<b>Aspect ratio</b>"
#~ msgstr "<b>Proporcje</b>"

#~ msgid "Video format"
#~ msgstr "Format wideo"

#~ msgid "No rotation"
#~ msgstr "Bez obracania"

#~ msgid "Rotate 180 degrees"
#~ msgstr "Obróć o 180 stopni"

#~ msgid "Rotate 90 degrees clockwise"
#~ msgstr "Obróć o 90 stopni zgodnie z kierunkiem ruchu wskazówek zegara"

#~ msgid "Rotate 90 degrees counter-clockwise"
#~ msgstr "Obróć o 90 stopni przeciwnie do kierunku ruchu wskazówek zegara"

#~ msgid "<b>Rotation</b>"
#~ msgstr "<b>Obracanie</b>"

#~ msgid "Horizontal mirror"
#~ msgstr "Odbicie poziome"

#~ msgid "Vertical mirror"
#~ msgstr "Odbicie pionowe"

#~ msgid "<b>Mirror</b>"
#~ msgstr "<b>Odbicie</b>"

#~ msgid "Swap fields"
#~ msgstr "Zamienia pola"

#~ msgid "<b>Field order</b>"
#~ msgstr "<b>Porządek rzeczy</b>"

#~ msgid "Add black bars"
#~ msgstr "Dodaj czarne pasy"

#~ msgid "Scale picture"
#~ msgstr "Skaluj obrazek"

#~ msgid "<b>Scaling mode</b>"
#~ msgstr "<b>Tryb skalowania</b>"

#~ msgid "Video options"
#~ msgstr "Opcje wideo"

#~ msgid "Use MBCMP (faster)"
#~ msgstr "Wykorzystaj MBCMP (szybsze)"

#~ msgid "Select MBD mode which needs the fewest bits"
#~ msgstr "Wybierz tryb MBD który potrzebuje najmniej bitów"

#~ msgid ""
#~ "Select the MBD mode which has the best rate distortion (better quality)"
#~ msgstr ""
#~ "Wybierz tryb MBD , który ma najlepszy algorytm zmian szybkości (lepsza "
#~ "jakość)"

#~ msgid ""
#~ "Use Trellis Searched Quantization (better quality, slower conversion)"
#~ msgstr ""
#~ "Użyj wyszukiwania kwantyzacji Trellis (lepsza jakość, wolniejsza "
#~ "konwersja)"

#~ msgid ""
#~ "<b>MacroBlock decision algorithm and Trellis searched quantization</b>"
#~ msgstr ""
#~ "<b>Algorytm decyzyjny MacroBlock i wyszukiwanie kwantyzacji Trellis</b>"

#~ msgid "Use dual pass encoding (better quality, much slower conversion)"
#~ msgstr ""
#~ "Wykonaj dwa przejścia kodowania (lepsza jakość, dużo wolniejsza konwersja)"

#~ msgid "Turbo 1st pass"
#~ msgstr "Tryb Turbo pierwszego przejścia"

#~ msgid "<b>Two pass encoding</b>"
#~ msgstr "<b>Dwa przejścia kodowania<b>"

#~ msgid "Median deinterlacing filter"
#~ msgstr "Filtr usuwania przeplotu Median"

#~ msgid "Don't deinterlace"
#~ msgstr "Nie usuwaj przeplotu"

#~ msgid "YADIF deinterlacing filter"
#~ msgstr "Filtr usuwania przeplotu YADIF"

#~ msgid "FFMpeg deinterlacing filter"
#~ msgstr "Filtr usuwania przeplotu FFMpeg"

#~ msgid "Linear blend"
#~ msgstr "Przejście liniowe"

#~ msgid "Lowpass5 deinterlacing filter"
#~ msgstr "Filtr usuwania przeplotu Lowpass5"

#~ msgid "<b>Deinterlacing</b>"
#~ msgstr "<b>Usuwanie przeplotu</b>"

#~ msgid "Quality"
#~ msgstr "Opcje jakości"

#~ msgid "Audio delay (in seconds):"
#~ msgstr "Opóźnienie audio (w sekundach):"

#~ msgid "Create DVD with 5.1 channel sound"
#~ msgstr "Utwórz DVD z dźwiękiem 5.1"

#~ msgid ""
#~ "This file already has AC3 sound (copy audio data instead of recompress it)"
#~ msgstr "Ten plik posiada już dźwięk AC3 (kopiuj dane audio bez rekompresji)"

#~ msgid "<b>Audio</b>"
#~ msgstr "<b>Audio</b>"

#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "This file is already a DVD/xCD-suitable MPEG-PS file"
#~ msgstr "To już jest odpowiedni plik DVD/xCD MPEG-PS"

#~ msgid "Repack audio and video without reencoding (useful for VOB files)"
#~ msgstr ""
#~ "Załaduj audio i video bez przekodowywania (użyteczne dla plików VOB)"

#~ msgid "Use a GOP of 12 frames (improves compatibility)"
#~ msgstr "Użyj 12 ramek GOP (poprawia kompatybilność)"

#~ msgid "<b>Special</b>"
#~ msgstr "<b>Specjalne</b>"

#~ msgid "Main (-X -Y)"
#~ msgstr "Główne (-X -Y)"

#~ msgid "-VF (X,Y)"
#~ msgstr "-VF (X,Y)"

#~ msgid "-LAVCOPTS (X:Y)"
#~ msgstr "-LAVCOPTS (X:Y)"

#~ msgid "-LAMEOPTS (X:Y)"
#~ msgstr "-LAMEOPTS (X:Y)"

#, fuzzy
#~ msgid "<b>Extra parameters for coder</b>"
#~ msgstr "<b>Dodatkowe parametry dla programu Mencoder</b>"

#~ msgid "Misc"
#~ msgstr "Różne"

#~ msgid "Advanced options"
#~ msgstr "Zaawansowane opcje"

#~ msgid "Preview"
#~ msgstr "Podgląd"

#~ msgid ""
#~ "Choose the folder where DeVeDe will create the DVD image and a name for "
#~ "it. Don't use a folder in a VFAT/FAT32 drive. Characters /, | and \\ are "
#~ "not allowed and will be replaced by underscores."
#~ msgstr ""
#~ "Wybierz folder w którym DeVeDe utworzy obraz płyty DVD i nadaj mu nazwę. "
#~ "Nie używaj folderów na dyskach z systemem plików VFAT/FAT32. Znaki /, | i "
#~ "\\ nie są dozwolone i będą zastąpione przez podkreślenie."

#~ msgid "Choose a folder"
#~ msgstr "Wybierz folder"

#~ msgid "Load a disc structure"
#~ msgstr "Wczytaj strukturę dysku"

#~ msgid "Warning: you already have a disc structure"
#~ msgstr "Ostrzeżenie: masz już gotową strukturę dysku"

#~ msgid ""
#~ "If you load a disc structure you will loose your current structure "
#~ "(unless you saved it). Continue?"
#~ msgstr ""
#~ "Jeśli wczytasz strukturę dysku, stracisz aktualną strukturę (jeśli jej "
#~ "niezapisano). Kontynuować?"

#~ msgid "185 MB CD"
#~ msgstr "185 MB CD"

#~ msgid "650 MB CD"
#~ msgstr "650 MB CD"

#~ msgid "700 MB CD"
#~ msgstr "700 MB CD"

#~ msgid "1.4 GB DVD"
#~ msgstr "1.4 GB DVD"

#~ msgid "4.7 GB DVD"
#~ msgstr "4.7 GB DVD"

#~ msgid "8.5 GB DVD"
#~ msgstr "8.5 GB DVD"

#~ msgid "DeVeDe"
#~ msgstr "DeVeDe"

#~ msgid "_File"
#~ msgstr "Plik"

#, fuzzy
#~ msgid "_Edit"
#~ msgstr "Edytuj"

#~ msgid "_Preferences"
#~ msgstr "Preferencje"

#~ msgid "_Help"
#~ msgstr "Pomoc"

#~ msgid "<b>Titles</b>"
#~ msgstr "<b>Tytuły</b>"

#~ msgid "<b>Files</b>"
#~ msgstr "<b>Pliki</b>"

#~ msgid "Length (seconds):"
#~ msgstr "Długość (sekundy):"

#~ msgid "Video rate (Kbits/sec):"
#~ msgstr "Szybkość transmisji wideo (Kbits/sec):"

#~ msgid "Audio rate (Kbits/sec):"
#~ msgstr "Szybkość transmisji audio (Kbits/sec):"

#~ msgid "Estimated length (MBytes):"
#~ msgstr "Przybliżona wielkość (Megabajty):"

#~ msgid "Output aspect ratio:"
#~ msgstr "Proporcja wyjściowa:"

#~ msgid "Size of chapters:"
#~ msgstr "Wielkość rozdziałów:"

#~ msgid "Adjust disc usage"
#~ msgstr "Modyfikuj użycie dysku"

#~ msgid "0.0%"
#~ msgstr "0.0%"

#~ msgid "Media size:"
#~ msgstr "Rozmiar nośnika:"

#~ msgid "<b>Disc usage</b>"
#~ msgstr "<b>Wykorzystanie dysku</b>"

#~ msgid "PAL"
#~ msgstr "PAL"

#~ msgid "<b>Default format</b>"
#~ msgstr "<b>Domyślny format</b>"

#~ msgid "Create a menu with the titles"
#~ msgstr "Utwórz menu z tytułami"

#~ msgid "Menu options"
#~ msgstr "Opcje menu"

#~ msgid "Preview menu"
#~ msgstr "Podgląd menu"

#~ msgid "<b>Menus</b>"
#~ msgstr "<b>Menu</b>"

#~ msgid "Only convert film files to compliant MPEG files"
#~ msgstr "Konwertuj tylko pliki z filmami do formatu plików MPEG"

#~ msgid "Create disc structure"
#~ msgstr "Tworzenie struktury dysku"

#~ msgid "Create an ISO or BIN/CUE image, ready to burn to a disc"
#~ msgstr "Utwórz obraz ISO lub BIN/CUE, gotowy do nagrania na nośniku"

#~ msgid "<b>Action</b>"
#~ msgstr "<b>Akcja</b>"

#~ msgid "Menu preview"
#~ msgstr "Podgląd menu"

#~ msgid "Shadow color:"
#~ msgstr "Kolor cienia:"

#~ msgid "Text color:"
#~ msgstr "Kolor tekstu:"

#~ msgid "Text:"
#~ msgstr "Tekst:"

#~ msgid "Font:"
#~ msgstr "Czcionka:"

#~ msgid "<b>Menu title</b>"
#~ msgstr "<b>Tytuł menu</b>"

#~ msgid "Select a picture to be used as background for disc's menu"
#~ msgstr "Wybierz obrazek który będzie użyty jako tło w menu dysku"

#~ msgid "Set default background"
#~ msgstr "Ustaw domyślne tło"

#~ msgid "<b>Menu background (only PNG)</b>"
#~ msgstr "<b>Tło menu (tylko PNG)</b>"

#~ msgid "Choose a music file"
#~ msgstr "Wybierz plik muzyczny"

#~ msgid "Set menus without sound"
#~ msgstr "Ustaw menu bez dźwięku"

#~ msgid "<b>Menu music</b>"
#~ msgstr "<b>Menu muzyki</b>"

#, fuzzy
#~ msgid "<b>Horizontal</b>"
#~ msgstr "<b>Położenie w poziomie</b>"

#, fuzzy
#~ msgid "<b>Vertical</b>"
#~ msgstr "<b>Specjalne</b>"

#~ msgid "Top"
#~ msgstr "Góra"

#~ msgid "Middle"
#~ msgstr "Środek"

#~ msgid "Bottom"
#~ msgstr "Dół"

#~ msgid "Left"
#~ msgstr "Lewa strona"

#~ msgid "Center"
#~ msgstr "Środek"

#~ msgid "Right"
#~ msgstr "Prawa strona"

#, fuzzy
#~ msgid "<b>Margins (%)</b>"
#~ msgstr "<b>Menu</b>"

#~ msgid "<b>Menu position</b>"
#~ msgstr "<b>Pozycja menu</b>"

#~ msgid "Color for unselected titles:"
#~ msgstr "Kolor dla niewybranych tytułów:"

#~ msgid "Color for shadows:"
#~ msgstr "Kolor cieni:"

#~ msgid "Color for selected title:"
#~ msgstr "Kolor dla wybranych tytułów:"

#~ msgid "Background color for titles:"
#~ msgstr "Kolor tła dla tytułów:"

#~ msgid "<b>Menu font and colors</b>"
#~ msgstr "<b>Kolor i czcionka menu</b>"

#~ msgid "Show menu at disc startup"
#~ msgstr "Pokaż menu dysku przy starcie"

#~ msgid "Jump to the first title at startup"
#~ msgstr "Przejdź do pierwszego tytułu podczas startu"

#~ msgid "<b>Disc startup options</b>"
#~ msgstr "<b>Opcje startowe dysku</b>"

#~ msgid "Can't find the font for subtitles. Aborting."
#~ msgstr "Nie mogę znaleźć czcionek dla napisów. Przerywam."

#~ msgid "Replay the preview again?"
#~ msgstr "Powtórzyć podgląd jeszcze raz?"

#~ msgid "<b>Preview video</b>"
#~ msgstr "<b>Podgląd video</b>"

#~ msgid ""
#~ "DeVeDe will create a preview with the selected parameters, so you will be "
#~ "able to check video quality, audio sync and so on."
#~ msgstr ""
#~ "DeVeDe utworzy podgląd z wybranymi parametrami, tak że możliwe będzie "
#~ "sprawdzenie jakości video, synchronizacji audio itd."

#~ msgid "Preview length:"
#~ msgstr "Długość podglądu:"

#~ msgid "Can't find the following programs:"
#~ msgstr "Nie można znaleźć następujących programów:"

#~ msgid ""
#~ "DeVeDe needs them in order to work. If you want to use DeVeDe, you must "
#~ "install all of them."
#~ msgstr ""
#~ "DeVeDe potrzebuje tych programów aby poprawnie działać. Jeśli chcesz "
#~ "używać DeVeDe, musisz je zainstalować."

#~ msgid "Creating..."
#~ msgstr "Tworzenie..."

#~ msgid "Save current disc structure"
#~ msgstr "Zapisz aktualną strukturę dysku"

#~ msgid "Title properties"
#~ msgstr "Właściwości tytułu"

#~ msgid "<b>Title's name</b>"
#~ msgstr "<b>Nazwy tytułów</b>"

#~ msgid "Stop reproduction/show disc menu"
#~ msgstr "Zatrzymaj odtwarzanie/pokaż menu dysku"

#~ msgid "Play the first title"
#~ msgstr "Odtwórz pierwszy tytuł"

#~ msgid "Play the previous title"
#~ msgstr "Odtwórz poprzedni tytuł"

#~ msgid "Play this title again (loop)"
#~ msgstr "Odtwórz ten tytuł jeszcze raz (pętla)"

#~ msgid "Play the next title"
#~ msgstr "Odtwórz następny tytuł"

#~ msgid "Play the last title"
#~ msgstr "Odtwórz ostatni tytuł"

#~ msgid "<b>Action to perform when this title ends</b>"
#~ msgstr "<b>Działanie do wykonania gdy ten tytuł zakończy się</b>"

#~ msgid "Warning"
#~ msgstr "Ostrzeżenie"

#~ msgid "<b>Vertical alignment</b>"
#~ msgstr "<b>Położenie w pionie</b>"

#~ msgid "Converting files from title"
#~ msgstr "Konwertowanie plików tytułu"

#~ msgid "gtk-cancel"
#~ msgstr "gtk-przerwij"

#~ msgid "gtk-ok"
#~ msgstr "gtk-ok"

#~ msgid "<b>Trellis searched quantization</b>"
#~ msgstr "<b>Wyszukana kwantyzacja Trellis</b>"

#~ msgid "gtk-no"
#~ msgstr "gtk-nie"

#~ msgid "gtk-yes"
#~ msgstr "gtk-tak"

#~ msgid "gtk-add"
#~ msgstr "gtk-dodaj"

#~ msgid "gtk-help"
#~ msgstr "gtk-pomoc"

#~ msgid "gtk-remove"
#~ msgstr "gtk-usuń"

#~ msgid "gtk-go-back"
#~ msgstr "gtk-idź wstecz"

#~ msgid "gtk-go-forward"
#~ msgstr "gtk-idź naprzód"

#~ msgid "gtk-open"
#~ msgstr "gtk-otwórz"

#~ msgid ""
#~ "Uses multiple threads with Mencoder, allowing a faster compression "
#~ "process. But requires Mencoder 1.0 RC2 or later."
#~ msgstr ""
#~ "Użycie wielowątkowości przez program Mencoder pozwala na szybszą "
#~ "kompresję. Wymaga wersji programu Mencoder 1.0 RC2 lub nowszej."

#~ msgid "gtk-about"
#~ msgstr "gtk-o"

#~ msgid "gtk-go-down"
#~ msgstr "gtk-idź w dół"

#~ msgid "gtk-go-up"
#~ msgstr "gtk-idź w górę"

#~ msgid "gtk-new"
#~ msgstr "gtk-nowy"

#~ msgid "gtk-properties"
#~ msgstr "gtk-właściwości"

#~ msgid "gtk-quit"
#~ msgstr "gtk-wyjdź"

#~ msgid "gtk-save"
#~ msgstr "gtk-zapisz"

#~ msgid "gtk-save-as"
#~ msgstr "gtk-zapisz jako"

#~ msgid "Super VideoCD"
#~ msgstr "Super VideoCD"

#~ msgid "Video DVD"
#~ msgstr "DVD wideo"

#, fuzzy
#~ msgid "VideoCD"
#~ msgstr "VideoCD"

#~ msgid ""
#~ "<b>Preview video</b>\n"
#~ "\n"
#~ "DeVeDe will create a preview with the selected parameters, so you will be "
#~ "able to check video quality, audio sync and so on.\n"
#~ "\n"
#~ "Choose how many seconds of film you want to convert to create the "
#~ "preview, and the directory where you want to store the temporary file "
#~ "(default is /var/tmp)."
#~ msgstr ""
#~ "<b>Podgląd wideo</b>\n"
#~ "\n"
#~ "DeVeDe utworzy podgląd zgodnie z ustawionymi parametrami, więc będzie "
#~ "można sprawdzić jakość wideo, synchronizację audio itp... .\n"
#~ "\n"
#~ "Wybierz ile sekund filmu chcesz przekonwertować na podgląd, oraz katalog "
#~ "w którym będą zapisane tymczasowe pliki (domyślnie to /var/tmp)."

#~ msgid ""
#~ "A CD with video in MPEG-1 format, with quality equivalent to VHS. Stores "
#~ "up to 80 minutes in a 80-minutes CD-R. Playable in all DVD players."
#~ msgstr ""
#~ "CD z wideo w formacie MPEG-1 o jakości odpowiadającej VHS. Można zapisać "
#~ "do 80 minut na 80-minutowej CD-R. Do odtwarzania we wszystkich "
#~ "odtwarzaczach DVD."

#~ msgid ""
#~ "A CD with video in MPEG-2 format and a resolution of 352x480 (or 352x576 "
#~ "in PAL). Playable in most of DVD players."
#~ msgstr ""
#~ "CD z wideo w formacie MPEG-2 i rozdzielczości 352x480 (lub 352x576 w "
#~ "PAL). Do odtwarzania w większości odtwarzaczy DVD."

#~ msgid ""
#~ "A CD with video in MPEG-2 format and a resolution of 480x480 pixels "
#~ "(480x576 in PAL). Playable in nearly all DVD players."
#~ msgstr ""
#~ "CD z wideo w formacie MPEG-2 i rozdzielczości 480x480 pikseli (480x576 w "
#~ "PAL). Do odtwarzania na prawie wszystkich odtwarzaczach DVD."

#~ msgid "A classic Video DVD, playable in all DVD players."
#~ msgstr "Klasyczna płyta DVD do odtwarzania we wszystkich odtwarzaczach DVD."

#~ msgid "Output video format:"
#~ msgstr "Wyjściowy format wideo:"

#~ msgid "There was an error while importing the file:"
#~ msgstr "Wystąpił błąd podczas importowania pliku:"
